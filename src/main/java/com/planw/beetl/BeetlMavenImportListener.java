package com.planw.beetl;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.planw.beetl.utils.ConstUtil;
import java.util.Collection;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.idea.maven.project.MavenImportListener;
import org.jetbrains.idea.maven.project.MavenProject;

public class BeetlMavenImportListener implements MavenImportListener {

  private static final Logger logger = Logger.getInstance(BeetlMavenImportListener.class);

  @Override
  public void importFinished(@NotNull Collection<MavenProject> collection,
      @NotNull List<Module> list) {

    logger.info("Maven发生变动，重新导入");
    Application application = ApplicationManager.getApplication();
    application.runReadAction(() -> {
      ConstUtil.changeReadyState();
      list.stream().map(Module::getProject).findFirst()
          .ifPresent(ConstUtil::initBeetlSql);
    });
  }

}
