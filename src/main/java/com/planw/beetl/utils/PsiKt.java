package com.planw.beetl.utils;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.util.PsiUtil;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.intellij.plugins.markdown.lang.MarkdownElementTypes;

public class PsiKt {

  private static final List<String> primitiveClasses = new ArrayList<>();

  static {
    primitiveClasses.add(Integer.TYPE.getCanonicalName());
    primitiveClasses.add(Integer.class.getCanonicalName());
    primitiveClasses.add(Double.TYPE.getCanonicalName());
    primitiveClasses.add(Double.class.getCanonicalName());
    primitiveClasses.add(Float.TYPE.getCanonicalName());
    primitiveClasses.add(Float.class.getCanonicalName());
    primitiveClasses.add(Short.TYPE.getCanonicalName());
    primitiveClasses.add(Short.class.getCanonicalName());
    primitiveClasses.add(Byte.TYPE.getCanonicalName());
    primitiveClasses.add(Byte.class.getCanonicalName());
    primitiveClasses.add(Character.TYPE.getCanonicalName());
    primitiveClasses.add(Character.class.getCanonicalName());
    primitiveClasses.add(Long.TYPE.getCanonicalName());
    primitiveClasses.add(Long.class.getCanonicalName());
    primitiveClasses.add(Boolean.TYPE.getCanonicalName());
    primitiveClasses.add(Boolean.class.getCanonicalName());
    primitiveClasses.add(String.class.getCanonicalName());
//    包括date
    primitiveClasses.add(Date.class.getCanonicalName());
    primitiveClasses.add(java.sql.Date.class.getCanonicalName());
    primitiveClasses.add(LocalDate.class.getCanonicalName());
    primitiveClasses.add(LocalDateTime.class.getCanonicalName());
    primitiveClasses.add(LocalTime.class.getCanonicalName());
  }

  /**
   * 是否是基础类型，包括包装类和String
   */
  public static boolean isPrimitive(PsiType psiType) {

    return primitiveClasses.contains(getClassName(psiType)) || primitiveClasses
        .contains(psiType.getCanonicalText());
  }

  /**
   * 是否是基础类型，包括包装类和String
   */
  public static String getClassName(PsiType psiType) {

    PsiClass psiClass = PsiUtil.resolveClassInType(psiType);
    if (psiClass == null) {
      return psiType.getCanonicalText();
    }
    return Optional.of(psiClass).map(PsiClass::getQualifiedName).orElse(StringUtils.EMPTY);
  }

  public static PsiElement findParentOfType(PsiElement node, IElementType parentType) {

    while (ObjectUtils
        .notEqual(
            Optional.ofNullable(node).map(PsiElement::getNode)
                .map(ASTNode::getElementType).orElse(null),
            MarkdownElementTypes.MARKDOWN_FILE
        )) {
      node = Optional.ofNullable(node).map(PsiElement::getParent).orElse(null);
    }
    return node;
  }

  /**
   * 查找直接子节点
   */
  public static <T> T findChildByClass(PsiElement psiElement, Class<T> aClass) {

    for (PsiElement cur = psiElement.getFirstChild(); cur != null; cur = cur.getNextSibling()) {
      if (aClass.isInstance(cur)) {
        return (T) cur;
      }
    }
    return null;
  }

}