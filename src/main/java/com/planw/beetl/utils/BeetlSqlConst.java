package com.planw.beetl.utils;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiPackage;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BeetlSqlConst {

  public String BEETL_SQL_PATH = "sql/";

  public String DELIMITER_PLACEHOLDER_START = "#{";

  public String DELIMITER_PLACEHOLDER_END = "}";

  public String DELIMITER_STATEMENT_START = "-- @";

  public String DELIMITER_STATEMENT_END = "";

  public String DELIMITER_PLACEHOLDER_START2 = "${";

  public String DELIMITER_PLACEHOLDER_END2 = "}";

  public String BASE_MAPPER_BEETL_CLASS = "org.beetl.sql.core.mapper.BaseMapper";

  public String BASE_MAPPER_BEETL_PACKAGE = "org.beetl.sql.core.mapper";

  public String SQL_RESOURCE_BEETL_CLASS = "org.beetl.sql.core.annotatoin.SqlResource";

  public String PARAM_BEETL_CLASS = "org.beetl.sql.core.annotatoin.Param";

  public String PAGE_QUERY_BEETL_CLASS = "org.beetl.sql.core.engine.PageQuery";

  public String PAGE_REQUEST_BEETL_CLASS = "org.beetl.sql.core.page.PageRequest";

  public String ROOT_BEETL_CLASS = "org.beetl.sql.mapper.annotation.Root";

  public PsiClass BASE_MAPPER_PSI_CLASS = null;

  public VirtualFile defaultPropertiesFile;

  public VirtualFile extPropertiesFile;

  public BeetlSqlConst() {

  }

  public BeetlSqlConst(Project project) {

    handle(project);
  }

  protected void handle(Project project) {

    if (project == null) {
      return;
    }
    JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
    PsiPackage psiPackage = javaPsiFacade.findPackage(BASE_MAPPER_BEETL_PACKAGE);
    if (psiPackage == null) {
      return;
    }
    for (PsiClass psiClass : psiPackage.getClasses()) {
      if (StringUtil.equals(psiClass.getQualifiedName(), BASE_MAPPER_BEETL_CLASS)) {
        this.BASE_MAPPER_PSI_CLASS = psiClass;
        break;
      }
    }
    defaultPropertiesFile = FilenameIndex.getVirtualFilesByName("btsql.properties", true,
        GlobalSearchScope.allScope(project)).stream().findFirst().orElse(null);
    handleProperties(defaultPropertiesFile);
    extPropertiesFile = FilenameIndex.getVirtualFilesByName("btsql-ext.properties", true,
        GlobalSearchScope.allScope(project)).stream().findFirst().orElse(null);
    handleProperties(extPropertiesFile);
  }

  private void handleProperties(VirtualFile propertiesFile) {

    if (propertiesFile == null) {
      return;
    }
    try (InputStream inputStream = propertiesFile.getInputStream();) {
      Properties properties = new Properties();
      properties.load(inputStream);
      DELIMITER_STATEMENT_START = properties.getProperty("DELIMITER_STATEMENT_START",
          DELIMITER_STATEMENT_START);
      DELIMITER_STATEMENT_END = properties.getProperty("DELIMITER_STATEMENT_END",
          DELIMITER_STATEMENT_END);
      DELIMITER_PLACEHOLDER_START = properties.getProperty("DELIMITER_PLACEHOLDER_START",
          DELIMITER_PLACEHOLDER_START);
      DELIMITER_PLACEHOLDER_END = properties.getProperty("DELIMITER_PLACEHOLDER_END",
          DELIMITER_PLACEHOLDER_END);
      DELIMITER_PLACEHOLDER_START2 = properties.getProperty("DELIMITER_PLACEHOLDER_START2",
          DELIMITER_PLACEHOLDER_START2);
      DELIMITER_PLACEHOLDER_END2 = properties.getProperty("DELIMITER_PLACEHOLDER_END2",
          DELIMITER_PLACEHOLDER_END2);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
