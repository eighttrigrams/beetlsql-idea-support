package com.planw.beetl.utils;


import com.intellij.openapi.project.Project;

public class BeetSql3Const extends BeetlSqlConst {

  public BeetSql3Const(Project project) {

    super.BASE_MAPPER_BEETL_CLASS = "org.beetl.sql.mapper.BaseMapper";
    super.BASE_MAPPER_BEETL_PACKAGE = "org.beetl.sql.mapper";
    super.SQL_RESOURCE_BEETL_CLASS = "org.beetl.sql.mapper.annotation.SqlResource";
    super.PARAM_BEETL_CLASS = "org.beetl.sql.mapper.annotation.Param";
    super.PAGE_QUERY_BEETL_CLASS = "org.beetl.sql.core.page.PageReqeust";
    handle(project);
  }

}
