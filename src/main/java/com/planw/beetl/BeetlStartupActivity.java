package com.planw.beetl;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.startup.ProjectActivity;
import com.planw.beetl.utils.ConstUtil;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BeetlStartupActivity implements ProjectActivity {

  private static final Logger logger = Logger.getInstance(BeetlStartupActivity.class);

  @Nullable
  @Override
  public Object execute(@NotNull Project project,
      @NotNull Continuation<? super Unit> continuation) {
    // 标识项目启动完成
    Application application = ApplicationManager.getApplication();
    application.runReadAction(() -> {
      ConstUtil.changeReadyState();
      ConstUtil.initBeetlSql(project);
    });
    logger.info("项目启动完成");
    return null;
  }

}
