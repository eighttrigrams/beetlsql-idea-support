package com.planw.beetl.model;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiType;
import java.util.ArrayList;
import java.util.List;

public class TreeNode {

  /**
   * 节点对应类型
   */
  private PsiType type;

  /**
   * 节点所在psi tree的元素
   */
  private PsiElement psiElement;

  /**
   * 对应的{@code type} 字段的显示名
   */
  private String path = "";

  /**
   * 当前节点及子节点是否显示在代码提示中
   */
  private boolean isShow;


  private List<TreeNode> children = new ArrayList<>();


  public List<TreeNode> getChildren() {

    return children;
  }

  public void setChildren(List<TreeNode> children) {

    this.children = children;
  }

  public void addChild(TreeNode node) {

    this.children.add(node);
  }

  public PsiType getType() {

    return type;
  }

  public void setType(PsiType type) {

    this.type = type;
  }

  public String getPath() {

    return path;
  }

  public void setPath(String path) {

    this.path = path;
  }

  public boolean isShow() {

    return isShow;
  }

  public void setShow(boolean show) {

    isShow = show;
  }

  public PsiElement getPsiElement() {

    return psiElement;
  }

  public void setPsiElement(PsiElement psiElement) {

    this.psiElement = psiElement;
  }

}
