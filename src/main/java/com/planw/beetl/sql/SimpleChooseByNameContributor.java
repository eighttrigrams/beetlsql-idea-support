package com.planw.beetl.sql;

import com.intellij.navigation.ChooseByNameContributorEx;
import com.intellij.navigation.NavigationItem;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.Processor;
import com.intellij.util.indexing.FindSymbolParameters;
import com.intellij.util.indexing.IdFilter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SimpleChooseByNameContributor implements ChooseByNameContributorEx {


  @Override
  public void processNames(@NotNull Processor<? super String> processor,
      @NotNull GlobalSearchScope globalSearchScope, @Nullable IdFilter idFilter) {

  }

  @Override
  public void processElementsWithName(@NotNull String name,
      @NotNull Processor<? super NavigationItem> processor,
      @NotNull FindSymbolParameters parameters) {

  }


}