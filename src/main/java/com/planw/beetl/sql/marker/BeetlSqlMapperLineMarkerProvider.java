package com.planw.beetl.sql.marker;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiIdentifier;
import com.intellij.psi.PsiMethod;
import com.planw.beetl.service.BeetlMapperService;
import com.planw.beetl.service.BeetlSqlService;
import com.planw.beetl.utils.ConstUtil;
import com.planw.beetl.utils.ContextIcons;
import com.planw.beetl.utils.PsiKt;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.jetbrains.annotations.NotNull;

public class BeetlSqlMapperLineMarkerProvider extends RelatedItemLineMarkerProvider {

  private static final Logger logger = Logger.getInstance(BeetlSqlMapperLineMarkerProvider.class);

  @Override
  protected void collectNavigationMarkers(@NotNull PsiElement element,
      @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {

    logger.info("调用Mapper增加跳转");
    if (!ConstUtil.isReady()) {
      logger.info("项目未准备完成");
      return;
    }
    if (!(element instanceof PsiClass)) {
      return;
    }
    Project project = element.getProject();
    // 强制加载改变beetl不同版本的常量
    BeetlSqlService beetlSqlService = BeetlSqlService.getInstance(project);
    BeetlMapperService beetlMapperService = BeetlMapperService.getInstance(project);
    PsiClass psiClass = (PsiClass) element;
    if (!beetlMapperService.isBeetlMapper(psiClass)) {
      logger.info("当前类文件不属于接口或未继承beetl BaseMapper接口");
      return;
    }
    PsiFile sqlFile = beetlMapperService.findSqlFile(psiClass);

    if (sqlFile == null) {
      logger.info("无法找到对应的markdown文件");
      return;
    }

    Map<PsiMethod, PsiElement> methodAndSqlIdMap = beetlMapperService.getMethodAndSqlIdMap(sqlFile,
        psiClass, null);
    if (methodAndSqlIdMap == null) {
      logger.info("无法获取SqlId与接口方法的对应Map");
      return;
    }
    String sqlFileName = sqlFile.getName();
    Set<Entry<PsiMethod, PsiElement>> entrySet = methodAndSqlIdMap.entrySet();
    for (Entry<PsiMethod, PsiElement> entry : entrySet) {
      PsiElement psiElement = entry.getValue();
      NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(
              ContextIcons.MAPPER_LINE_MARKER_ICON).setAlignment(GutterIconRenderer.Alignment.CENTER)
          .setTargets(Collections.singleton(psiElement))
          .setTooltipTitle("Navigation to " + psiElement.getText() + " in " + sqlFileName);
      PsiIdentifier psiIdentifier = PsiKt.findChildByClass(entry.getKey(), PsiIdentifier.class);
      logger.info("创建md的" + psiElement + "内容与方法" + psiIdentifier + "名称的跳转图标");
      if (psiIdentifier != null) {
        result.add(builder.createLineMarkerInfo(psiIdentifier));
      }
    }
  }

}
