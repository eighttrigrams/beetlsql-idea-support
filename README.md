## 插件说明

idea打开项目后请等待项目加载完成，也就是常看到的进度条完成，项目目录载入完成。
这不只是这个插件需要这样，几乎涉及读取项目和依赖的东西，都需要等待项目载入完成。

这是idea的启动流程如此，idea中的顺序是application->project->module及其它。
项目基本上是每个插件的最早需要的单元。

在每个项目中，插件会自动读取maven依赖判断beetlsql版本，如果存在多个，则使用beetlsql3。
插件会随机读取多个beetsql.jar中的btlsql.properties的FN和TAG函数，作为代码完成的选项。
同时会随机读取btlsql-ext.properties的FN和TAG函数作为代码完成的选项。

所以这就要求一个项目中的beetlsql版本是唯一的，不管项目存在多少个模块。
并且btlsql-ext.properties也是唯一，意味着每个idea打开的项目就是一个spring或者springboot项目。

不支持gradle上的使用。gradle太麻烦了。

## Release 
- 1.1.0 
  1. 修复IDEA 插件开发的API兼容性问题，以长期支持idea-2023.1及以上版本，更低版本不再支持
  2. idea插件市场不太让id包括intellij单词，所以改了下插件id，所以可能会导致插件市场存在两个插件，请选择此1.1.0版本
  3. 优化了跳转和代码完成
  
  4. 问题：一个项目的多个模块能使用不同的BeetlSql版本嘛。
        > 答：不能，因为不好处理每个模块该读取哪个btl-ext.properties。
     而且在maven的依赖树中，作为一个基础模块可以被多个模块引入，这时候基础模块一般没有beetlsql依赖，这就导致不知道这个模块用哪个版本.

  5. 问题：为什么没有出现跳转？
        > 答：如果没有出现跳转的图标，请重新打开文件或者重新打开项目。
    这是因为插件的运行依赖idea查找maven依赖，这要求项目必须加载完成后才能进行。
    而且由于idea对读取的限制，这些操作都是在idea内置的一种专门执行读取操作的线程，所以在项目刚打开时是存在延迟的。

## 关于开发idea插件的流程，简单把jetbrains官网的东西介绍一下

有两种方式，第一种是不用任何项目依赖管理工具纯插件项目结构的方式，这种就不推荐了，因为官方自己都不大费功夫了，
个人猜测这种应该可以用maven的assembly插件处理成idea插件项目的目录。

第二种就是重点介绍的了，通过gradle来创建idea插件项目。
idea官方提供了一个gradle的插件用来处理插件开发的过程，包括但不限于常用的build，runIde，verify等。

其插件在build.gradle文件中的内容：

```groovy
plugins {
    id 'org.jetbrains.intellij' version "1.16.0"
}
```

而基于这个插件，以及gradle的版本，这里开始就有很大不同了。
在旧版本中，印象里应该是gradle7+gradle插件0.6.x的版本吧，它在下载idea运行时环境时非常痛苦，
虽然有办法解决，但很麻烦，最后会写怎么解决。所以真要开发的话，建议直接升级高版本吧。

----------------------------------
目前项目代码用的环境是gradle8.4+gradle插件1.16.0版本。

- 第一步，自然是安装gradle，剩下的内容参照我的build.gradle的内容
- 第二步，建议全新项目gradle转用kotlin作为脚本语言，gradle以前的groovy是真烂
- 第三步，着重解释一下官方gradle开发插件的使用，这个插件最重要的工作有两部分：
    - 一是下载idea运行时，这个运行时不同于我们正常下载的idea，它少了正常的启动exe等。
    - > 这里就是有问题了，gradle自己下载的话非常麻烦，可以手动在jetbrains的release网站下载对应版本的运行时。
      同时它也放出了官方的idea插件的源码等，当你用到其它的idea插件的时候就需要下载源码了。
      这个时候呢，新版本的gradle和gradle插件就比旧版的强了，gradle插件的localPath就能直接指定路径。
      而在旧版里面就需要一些骚操作了。
      在这个release网站中，搜索`com.jetbrains.intellij.idea` 会跳到idea运行时的位置，
      其中以`Build Number`那列为准，可以说`Version`是对外部，`Build Number`是对内部的，
      所以你会看到`Build Number`的值会重复两次，但version不一样，两个随便下哪个都行。
      其中下载的是` ideaIC.zip`和`ideaIC-sources.jar`，剩下的就是看build.gradle里面的配置内容了。
    - 二是处理自己开发插件时的一些依赖信息，以及build后生成对应plugin.xml内容，这部分可以直接不用它，直接写在plugin.xml中都行

### 关于旧版gradle和gradle开发插件的处理

还是先在release网站下载对应的idea运行时，但和新版不一样，旧版没有localPath，这时候就需要骚操作了。
先说下这个操作的原理，gradle在下载任何依赖时都是会在最后一级生成一个hash目录，比如下载`212.4746.92`
版本时会在gradle本地仓库目录中生成：
`gradle_repo\caches\modules-2\files-2.1\com.jetbrains.intellij.idea\ideaIC\212.4746.92\4ff7516024b4b547dc65b07c9a6763f2d6b6a627`。
这个hash只要你不删除这个目录，基本就是固定的了，我们需要做的就是，中断gradle的下载，删除这个hash目录里面的内容，
把自己下载的ideaIC.zip放进去，然后再刷新一下gradle启动下载过程，这时候gradle检测到里面已经有了文件，就会跳过下载过程，直接进行解压。
这个方法可能一次不能成功，这种情况下就得删除hash目录，多试几次了。


下载网站：[Idea 各版本下载网站](https://www.jetbrains.com/intellij-repository/releases)
